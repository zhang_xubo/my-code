"""

3rd: flask

"""
import os
import json
import logging


from flask import Flask, escape, request, render_template, send_file

app = Flask(__name__)

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

download_file_path = "D:\\"

@app.route('/')
def startup():
    return render_template("index.html")


@app.route('/rest/get_soft_list')
def get_software():
    ip = request.remote_addr
    logger.warning("Remote Addr [%s] | Access to Page" % (ip))
    files = os.listdir(download_file_path)
    filelist = []
    for file in files:
        if os.path.isfile(os.path.join(download_file_path, file)):
            filelist.append(file)
    return json.dumps(tuple(filelist))

@app.route('/rest/download_software', methods=['POST'])
def download_software():
    ip = request.remote_addr
    softname = request.json.get('name')
    logger.warning("Remote Addr [%s] | Download soft [%s]" % (ip, softname))
    return send_file('%s%s' % (download_file_path, softname), attachment_filename='%s' % softname, as_attachment=True)


if __name__ == '__main__':
   app.run(port=8080)